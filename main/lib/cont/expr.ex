defmodule Cont.Expr do

  defmodule Inode do
    defstruct value: 0
  end

  defimpl String.Chars, for: Inode do
    def to_string(d) do
      "{value: #{Integer.to_string(d.value)}}" 
    end
  end

end
