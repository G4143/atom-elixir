defmodule Main do

  def main(_args \\ []) do

    inode = %Cont.Expr.Inode{value: 1234}

    IO.inspect inode

    IO.puts "#{inode}"

  end
end
